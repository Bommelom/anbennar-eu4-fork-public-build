the_fallen_invasion_army = {
	H94 = { infantry = PREV }
	H94 = { infantry = PREV }
	H94 = { infantry = PREV }
	H94 = { infantry = PREV }
	H94 = { infantry = PREV }
	H94 = { infantry = PREV }
	H94 = { infantry = PREV }
	H94 = { infantry = PREV }
	H94 = { infantry = PREV }
	H94 = { infantry = PREV }
	H94 = { infantry = PREV }
	H94 = { infantry = PREV }
	H94 = { infantry = PREV }
	H94 = { infantry = PREV }
	H94 = { infantry = PREV }
	H94 = { infantry = PREV }
	H94 = { infantry = PREV }
	H94 = { infantry = PREV }
	H94 = { infantry = PREV }
	H94 = { infantry = PREV }
	H94 = { artillery = PREV }
	H94 = { artillery = PREV }
	H94 = { artillery = PREV }
	H94 = { artillery = PREV }
	H94 = { artillery = PREV }
	H94 = { artillery = PREV }
	H94 = { artillery = PREV }
	H94 = { artillery = PREV }
	H94 = { artillery = PREV }
	H94 = { artillery = PREV }
}

the_fallen_invasion_infiltrator = {
	H94 = { infantry = PREV }
	H94 = { infantry = PREV }
	H94 = { infantry = PREV }
	H94 = { infantry = PREV }
	H94 = { infantry = PREV }
	H94 = { infantry = PREV }
	H94 = { infantry = PREV }
	H94 = { infantry = PREV }
	H94 = { infantry = PREV }
	H94 = { infantry = PREV }
}


define_hold_to_repair = {
	random_owned_province = {
		limit = { OR = { has_province_modifier = ruined_hold has_province_modifier = partially_ruined_hold has_province_modifier = damaged_hold } NOT = { has_province_modifier = hold_being_restored has_province_flag = hold_@ROOT } is_city = yes }
		set_province_flag = option_hold_1_@ROOT
		set_province_flag = hold_@ROOT
		save_event_target_as = hold1
	}
	random_owned_province = {
		limit = { OR = { has_province_modifier = ruined_hold has_province_modifier = partially_ruined_hold has_province_modifier = damaged_hold } NOT = { has_province_modifier = hold_being_restored has_province_flag = hold_@ROOT } is_city = yes }
		set_province_flag = option_hold_2_@ROOT
		set_province_flag = hold_@ROOT
		save_event_target_as = hold2
	}
	random_owned_province = {
		limit = { OR = { has_province_modifier = ruined_hold has_province_modifier = partially_ruined_hold has_province_modifier = damaged_hold } NOT = { has_province_modifier = hold_being_restored has_province_flag = hold_@ROOT } is_city = yes }
		set_province_flag = option_hold_3_@ROOT
		set_province_flag = hold_@ROOT
		save_event_target_as = hold3
	}
	random_owned_province = {
		limit = { OR = { has_province_modifier = ruined_hold has_province_modifier = partially_ruined_hold has_province_modifier = damaged_hold } NOT = { has_province_modifier = hold_being_restored has_province_flag = hold_@ROOT } is_city = yes }
		set_province_flag = option_hold_4_@ROOT
		set_province_flag = hold_@ROOT
		save_event_target_as = hold4
	}
	random_owned_province = {
		limit = { OR = { has_province_modifier = ruined_hold has_province_modifier = partially_ruined_hold has_province_modifier = damaged_hold } NOT = { has_province_modifier = hold_being_restored has_province_flag = hold_@ROOT } is_city = yes }
		set_province_flag = option_hold_5_@ROOT
		set_province_flag = hold_@ROOT
		save_event_target_as = hold5
	}
	random_owned_province = {
		limit = { OR = { has_province_modifier = ruined_hold has_province_modifier = partially_ruined_hold has_province_modifier = damaged_hold } NOT = { has_province_modifier = hold_being_restored has_province_flag = hold_@ROOT } is_city = yes }
		set_province_flag = option_hold_6_@ROOT
		set_province_flag = hold_@ROOT
		save_event_target_as = hold6
	}
}

repair_hold = {
	if = {
		limit = { has_province_modifier = ruined_hold }
		trigger_switch = {
			on_trigger = development
			
			100 = { owner = { add_mil_power = -500 add_dip_power = -500 add_adm_power = -500 add_treasury = -3500 } }
			90 = { owner = { add_mil_power = -450 add_dip_power = -450 add_adm_power = -450 add_treasury = -3000 } }
			80 = { owner = { add_mil_power = -400 add_dip_power = -400 add_adm_power = -400 add_treasury = -2500 } }
			70 = { owner = { add_mil_power = -350 add_dip_power = -350 add_adm_power = -350 add_treasury = -2000 } }
			60 = { owner = { add_mil_power = -300 add_dip_power = -300 add_adm_power = -300 add_treasury = -1750 } }
			50 = { owner = { add_mil_power = -250 add_dip_power = -250 add_adm_power = -250 add_treasury = -1500 } }
			40 = { owner = { add_mil_power = -200 add_dip_power = -200 add_adm_power = -200 add_treasury = -1250 } }
			30 = { owner = { add_mil_power = -150 add_dip_power = -150 add_adm_power = -150 add_treasury = -1000 } }
			20 = { owner = { add_mil_power = -100 add_dip_power = -100 add_adm_power = -100 add_treasury = -400 } }
			10 = { owner = { add_mil_power = -50 add_dip_power = -50 add_adm_power = -50 add_treasury = -300 } }
			3 = { owner = { add_mil_power = -50 add_dip_power = -50 add_adm_power = -50 add_treasury = -250 } }
		}
	}
	else_if = {
		limit = { has_province_modifier = partially_ruined_hold }
		trigger_switch = {
			on_trigger = development
			
			100 = { owner = { add_mil_power = -400 add_dip_power = -400 add_adm_power = -400 add_treasury = -3000 } }
			90 = { owner = { add_mil_power = -350 add_dip_power = -350 add_adm_power = -350 add_treasury = -2500 } }
			80 = { owner = { add_mil_power = -325 add_dip_power = -325 add_adm_power = -325 add_treasury = -2000 } }
			70 = { owner = { add_mil_power = -300 add_dip_power = -300 add_adm_power = -300 add_treasury = -1500 } }
			60 = { owner = { add_mil_power = -250 add_dip_power = -250 add_adm_power = -250 add_treasury = -1250 } }
			50 = { owner = { add_mil_power = -200 add_dip_power = -200 add_adm_power = -200 add_treasury = -1000 } }
			40 = { owner = { add_mil_power = -175 add_dip_power = -175 add_adm_power = -175 add_treasury = -750 } }
			30 = { owner = { add_mil_power = -150 add_dip_power = -150 add_adm_power = -150 add_treasury = -500 } }
			20 = { owner = { add_mil_power = -100 add_dip_power = -100 add_adm_power = -100 add_treasury = -250 } }
			10 = { owner = { add_mil_power = -50 add_dip_power = -50 add_adm_power = -50 add_treasury = -150 } }
			3 = { owner = { add_mil_power = -50 add_dip_power = -50 add_adm_power = -50 add_treasury = -100 } }
		}
	}
	else_if = {
		limit = { has_province_modifier = damaged_hold }
		trigger_switch = {
			on_trigger = development
			
			100 = { owner = { add_mil_power = -300 add_dip_power = -300 add_adm_power = -300 add_treasury = -2500 } }
			90 = { owner = { add_mil_power = -275 add_dip_power = -275 add_adm_power = -275 add_treasury = -2000 } }
			80 = { owner = { add_mil_power = -250 add_dip_power = -250 add_adm_power = -250 add_treasury = -1500 } }
			70 = { owner = { add_mil_power = -225 add_dip_power = -225 add_adm_power = -225 add_treasury = -1250 } }
			60 = { owner = { add_mil_power = -175 add_dip_power = -175 add_adm_power = -175 add_treasury = -1000 } }
			50 = { owner = { add_mil_power = -125 add_dip_power = -125 add_adm_power = -125 add_treasury = -750 } }
			40 = { owner = { add_mil_power = -100 add_dip_power = -100 add_adm_power = -100 add_treasury = -500 } }
			30 = { owner = { add_mil_power = -75 add_dip_power = -75 add_adm_power = -75 add_treasury = -350 } }
			20 = { owner = { add_mil_power = -50 add_dip_power = -50 add_adm_power = -50 add_treasury = -200 } }
			10 = { owner = { add_mil_power = -50 add_dip_power = -50 add_adm_power = -50 add_treasury = -150 } }
			3 = { owner = { add_mil_power = -50 add_dip_power = -50 add_adm_power = -50 add_treasury = -100 } }
		}
	}
	
	add_province_modifier = {
		name = hold_being_restored
		duration = -1
	}
	hidden_effect = {
		set_province_flag = being_restored
		owner = {
			every_owned_province = {
				limit = { has_province_flag = hold_@ROOT }
				clr_province_flag = hold_@ROOT
				clr_province_flag = option_hold_1_@ROOT
				clr_province_flag = option_hold_2_@ROOT
				clr_province_flag = option_hold_3_@ROOT
				clr_province_flag = option_hold_4_@ROOT
				clr_province_flag = option_hold_5_@ROOT
				clr_province_flag = option_hold_6_@ROOT
			}
			clr_country_flag = open_repair_hold_menu
		}
	}
}

define_hold_to_clear = {
	random_owned_province = {
		limit = { has_province_modifier = infested_hold is_city = yes NOT = { has_province_flag = infested_@ROOT } }
		set_province_flag = option_infested_1_@ROOT
		set_province_flag = infested_@ROOT
		save_event_target_as = infested1
	}
	random_owned_province = {
		limit = { has_province_modifier = infested_hold is_city = yes NOT = { has_province_flag = infested_@ROOT } }
		set_province_flag = option_infested_2_@ROOT
		set_province_flag = infested_@ROOT
		save_event_target_as = infested2
	}
	random_owned_province = {
		limit = { has_province_modifier = infested_hold is_city = yes NOT = { has_province_flag = infested_@ROOT } }
		set_province_flag = option_infested_3_@ROOT
		set_province_flag = infested_@ROOT
		save_event_target_as = infested3
	}
	random_owned_province = {
		limit = { has_province_modifier = infested_hold is_city = yes NOT = { has_province_flag = infested_@ROOT } }
		set_province_flag = option_infested_4_@ROOT
		set_province_flag = infested_@ROOT
		save_event_target_as = infested4
	}
}

infested_hold_clearing = {
	trigger_switch = {
		on_trigger = development
		
		35 = { spawn_rebels = { type = cave_orc_rebel culture = black_orc size = 1 } spawn_rebels = { type = cave_orc_rebel culture = black_orc size = 1 } spawn_rebels = { type = cave_orc_rebel culture = black_orc size = 1 } }
		20 = { spawn_rebels = { type = cave_orc_rebel culture = black_orc size = 1 } spawn_rebels = { type = cave_orc_rebel culture = black_orc size = 1 } }
		3 = { spawn_rebels = { type = cave_orc_rebel culture = black_orc size = 1 } }
	}
	remove_province_modifier = infested_hold
	add_permanent_province_modifier = {
		name = ruined_hold
		duration = -1
	}
	hidden_effect = {
		owner = {
			every_owned_province = {
				limit = { has_province_flag = infested_@ROOT }
				clr_province_flag = infested_@ROOT
				clr_province_flag = option_infested_1_@ROOT
				clr_province_flag = option_infested_2_@ROOT
				clr_province_flag = option_infested_3_@ROOT
				clr_province_flag = option_infested_4_@ROOT
			}
			clr_country_flag = open_infested_hold_menu
		}
	}
}
setup_digging_deeper_capital = {
	hidden_effect = {
		trigger_switch = {
			on_trigger = has_province_modifier
			
			dig_1 = { set_variable = { speed = 1 } }
			dig_2 = { set_variable = { speed = 2 } }
			dig_3 = { set_variable = { speed = 2.5 } }
			dig_4 = { set_variable = { speed = 3 } }
			dig_5 = { set_variable = { speed = 3.5 } }
			dig_6 = { set_variable = { speed = 4 } }
			dig_7 = { set_variable = { speed = 4.5 } }
			dig_8 = { set_variable = { speed = 5 } }
			dig_9 = { set_variable = { speed = 5.5 } }
			dig_10 = { set_variable = { speed = 6 } }
			easy_rock = { multiply_variable = { speed = 0.95 } }
			orc_hold_raider = { multiply_variable = { speed = 1.2 } }
			orc_hold_barrack = { multiply_variable = { speed = 1.2 } }
			orc_hold_arena = { multiply_variable = { speed = 1.2 } }
		}

		if = {
			limit = { has_terrain = dwarven_hold continent = serpentspine }
			multiply_variable = { speed = 0.8 }
		}
		if = {
			limit = { province_id = 4119 }
			multiply_variable = { speed = 0.9 }
		}
		if = {
			limit = { province_id = 4311 }
			multiply_variable = { speed = 0.95 }
		}
		if = {
			limit = { owner = { has_country_flag = dig_through } }
			multiply_variable = { speed = 0.8 }
		}
		if = {
			limit = { owner = { has_country_flag = remnant_dig } }
			multiply_variable = { speed = 0.9 }
		}
		if = {
			limit = { owner = { has_country_flag = not_dig_through } }
			multiply_variable = { speed = 1.2 }
		}
		if = {
			limit = { owner = { has_country_modifier = how_to_dig } }
			multiply_variable = { speed = 0.9 }
		}
		if = {
			limit = { owner = { has_country_modifier = dadv_digging_expert } }
			multiply_variable = { speed = 0.9 }
		}
		if = {
			limit = { owner = { NOT = { has_country_modifier = dwarven_administration } } }
			multiply_variable = { speed = 1.5 }
		}
	}
}

start_digging_deeper = {
	hidden_effect = { cancel_construction = yes }
	if = {
		limit = { check_variable = { speed = 8 } }
		hidden_effect = { add_building_construction = { building = digging_deeper speed = 8 cost = 64 } }
		custom_tooltip = cost_capital_dig_tooltip_8
	}
	else_if = {
		limit = { check_variable = { speed = 7.5 } }
		hidden_effect = { add_building_construction = { building = digging_deeper speed = 7.5 cost = 56.25 } }
		custom_tooltip = cost_capital_dig_tooltip_7.5
	}
	else_if = {
		limit = { check_variable = { speed = 7 } }
		hidden_effect = { add_building_construction = { building = digging_deeper speed = 7 cost = 49 } }
		custom_tooltip = cost_capital_dig_tooltip_7
	}
	else_if = {
		limit = { check_variable = { speed = 6.5 } }
		hidden_effect = { add_building_construction = { building = digging_deeper speed = 6.5 cost = 42.25 } }
		custom_tooltip = cost_capital_dig_tooltip_6.5
	}
	else_if = {
		limit = { check_variable = { speed = 6 } }
		hidden_effect = { add_building_construction = { building = digging_deeper speed = 6 cost = 36 } }
		custom_tooltip = cost_capital_dig_tooltip_6
	}
	else_if = {
		limit = { check_variable = { speed = 5.5 } }
		hidden_effect = { add_building_construction = { building = digging_deeper speed = 5.5 cost = 30.25 } }
		custom_tooltip = cost_capital_dig_tooltip_5.5
	}
	else_if = {
		limit = { check_variable = { speed = 5 } }
		hidden_effect = { add_building_construction = { building = digging_deeper speed = 5 cost = 25 } }
		custom_tooltip = cost_capital_dig_tooltip_5
	}
	else_if = {
		limit = { check_variable = { speed = 4.5 } }
		hidden_effect = { add_building_construction = { building = digging_deeper speed = 4.5 cost = 20.25 } }
		custom_tooltip = cost_capital_dig_tooltip_4.5
	}
	else_if = {
		limit = { check_variable = { speed = 4 } }
		hidden_effect = { add_building_construction = { building = digging_deeper speed = 4 cost = 16 } }
		custom_tooltip = cost_capital_dig_tooltip_4
	}
	else_if = {
		limit = { check_variable = { speed = 3.5 } }
		hidden_effect = { add_building_construction = { building = digging_deeper speed = 3.5 cost = 12.25 } }
		custom_tooltip = cost_capital_dig_tooltip_3.5
	}
	else_if = {
		limit = { check_variable = { speed = 3 } }
		hidden_effect = { add_building_construction = { building = digging_deeper speed = 3 cost = 9 } }
		custom_tooltip = cost_capital_dig_tooltip_3
	}
	else_if = {
		limit = { check_variable = { speed = 2.5 } }
		hidden_effect = { add_building_construction = { building = digging_deeper speed = 2.5 cost = 6.25 } }
		custom_tooltip = cost_capital_dig_tooltip_2.5
	}
	else_if = {
		limit = { check_variable = { speed = 2 } }
		hidden_effect = { add_building_construction = { building = digging_deeper speed = 2 cost = 4 } }
		custom_tooltip = cost_capital_dig_tooltip_2
	}
	else_if = {
		limit = { check_variable = { speed = 1.5 } }
		hidden_effect = { add_building_construction = { building = digging_deeper speed = 1.5 cost = 2.25 } }
		custom_tooltip = cost_capital_dig_tooltip_1.5
	}
	else_if = {
		limit = { check_variable = { speed = 1 } }
		hidden_effect = { add_building_construction = { building = digging_deeper speed = 1 cost = 1 } }
		custom_tooltip = cost_capital_dig_tooltip_1
	}
	else_if = {
		limit = { check_variable = { speed = 0 } }
		hidden_effect = { add_building_construction = { building = digging_deeper speed = 1 cost = 1 } }
		custom_tooltip = cost_capital_dig_tooltip_1
	}
}

upgrade_hold_capital = {
	trigger_switch = {
		on_trigger = has_province_modifier
		
		dig_1 = { remove_province_modifier = dig_1 add_permanent_province_modifier = { name = dig_2 duration = -1 } }
		dig_2 = { remove_province_modifier = dig_2 add_permanent_province_modifier = { name = dig_3 duration = -1 } }
		dig_3 = { remove_province_modifier = dig_3 add_permanent_province_modifier = { name = dig_4 duration = -1 } }
		dig_4 = { remove_province_modifier = dig_4 add_permanent_province_modifier = { name = dig_5 duration = -1 } }
		dig_5 = { remove_province_modifier = dig_5 add_permanent_province_modifier = { name = dig_6 duration = -1 } }
		dig_6 = { remove_province_modifier = dig_6 add_permanent_province_modifier = { name = dig_7 duration = -1 } }
		dig_7 = { remove_province_modifier = dig_7 add_permanent_province_modifier = { name = dig_8 duration = -1 } }
		dig_8 = { remove_province_modifier = dig_8 add_permanent_province_modifier = { name = dig_9 duration = -1 } }
		dig_9 = { remove_province_modifier = dig_9 add_permanent_province_modifier = { name = dig_10 duration = -1 } }
		dig_10 = { remove_province_modifier = dig_10 add_permanent_province_modifier = { name = dig_11 duration = -1 } }
	}
	hidden_effect = { 
		clr_province_flag = digging_deeper
		owner = {
			clr_country_flag = is_digging
			clr_country_flag = dig_prod
			clr_country_flag = dig_manpower
			clr_country_flag = dig_culture
			clr_country_flag = dig_explosive
			remove_country_modifier = dig_prod
			remove_country_modifier = dig_manpower
			remove_country_modifier = dig_explosive
		}
		capital_scope = { 
			remove_province_modifier = dig_deeper
			remove_province_modifier = dig_deeper_ai
			hidden_effect = { clr_province_flag = dig_capital }
		}
	}
}

define_hold_that_can_dig = {
	random_owned_province = {
		limit = { can_upgrade_hold = yes NOT = { has_province_flag = hold_@ROOT } }
		set_province_flag = option_hold_1_@ROOT
		set_province_flag = hold_@ROOT
		save_event_target_as = hold1
		setup_digging_deeper_non_capital = yes
	}
	random_owned_province = {
		limit = { can_upgrade_hold = yes NOT = { has_province_flag = hold_@ROOT } }
		set_province_flag = option_hold_2_@ROOT
		set_province_flag = hold_@ROOT
		save_event_target_as = hold2
		setup_digging_deeper_non_capital = yes
	}
	random_owned_province = {
		limit = { can_upgrade_hold = yes NOT = { has_province_flag = hold_@ROOT } }
		set_province_flag = option_hold_3_@ROOT
		set_province_flag = hold_@ROOT
		save_event_target_as = hold3
		setup_digging_deeper_non_capital = yes
	}
	random_owned_province = {
		limit = { can_upgrade_hold = yes NOT = { has_province_flag = hold_@ROOT } }
		set_province_flag = option_hold_4_@ROOT
		set_province_flag = hold_@ROOT
		save_event_target_as = hold4
		setup_digging_deeper_non_capital = yes
	}
	random_owned_province = {
		limit = { can_upgrade_hold = yes NOT = { has_province_flag = hold_@ROOT } }
		set_province_flag = option_hold_5_@ROOT
		set_province_flag = hold_@ROOT
		save_event_target_as = hold5
		setup_digging_deeper_non_capital = yes
	}
	random_owned_province = {
		limit = { can_upgrade_hold = yes NOT = { has_province_flag = hold_@ROOT } }
		set_province_flag = option_hold_6_@ROOT
		set_province_flag = hold_@ROOT
		save_event_target_as = hold6
		setup_digging_deeper_non_capital = yes
	}
	random_owned_province = {
		limit = { can_upgrade_hold = yes NOT = { has_province_flag = hold_@ROOT } }
		set_province_flag = option_hold_7_@ROOT
		set_province_flag = hold_@ROOT
		save_event_target_as = hold7
		setup_digging_deeper_non_capital = yes
	}
	random_owned_province = {
		limit = { can_upgrade_hold = yes NOT = { has_province_flag = hold_@ROOT } }
		set_province_flag = option_hold_8_@ROOT
		set_province_flag = hold_@ROOT
		save_event_target_as = hold8
		setup_digging_deeper_non_capital = yes
	}
	random_owned_province = {
		limit = { can_upgrade_hold = yes NOT = { has_province_flag = hold_@ROOT } }
		set_province_flag = option_hold_9_@ROOT
		set_province_flag = hold_@ROOT
		save_event_target_as = hold9
		setup_digging_deeper_non_capital = yes
	}
	random_owned_province = {
		limit = { can_upgrade_hold = yes NOT = { has_province_flag = hold_@ROOT } }
		set_province_flag = option_hold_10_@ROOT
		set_province_flag = hold_@ROOT
		save_event_target_as = hold10
		setup_digging_deeper_non_capital = yes
	}
	random_owned_province = {
		limit = { can_upgrade_hold = yes NOT = { has_province_flag = hold_@ROOT } }
		set_province_flag = option_hold_11_@ROOT
		set_province_flag = hold_@ROOT
		save_event_target_as = hold11
		setup_digging_deeper_non_capital = yes
	}
	random_owned_province = {
		limit = { can_upgrade_hold = yes NOT = { has_province_flag = hold_@ROOT } }
		set_province_flag = option_hold_12_@ROOT
		set_province_flag = hold_@ROOT
		save_event_target_as = hold12
		setup_digging_deeper_non_capital = yes
	}
	random_owned_province = {
		limit = { can_upgrade_hold = yes NOT = { has_province_flag = hold_@ROOT } }
		set_province_flag = option_hold_13_@ROOT
		set_province_flag = hold_@ROOT
		save_event_target_as = hold13
		setup_digging_deeper_non_capital = yes
	}
	random_owned_province = {
		limit = { can_upgrade_hold = yes NOT = { has_province_flag = hold_@ROOT } }
		set_province_flag = option_hold_14_@ROOT
		set_province_flag = hold_@ROOT
		save_event_target_as = hold14
		setup_digging_deeper_non_capital = yes
	}
	random_owned_province = {
		limit = { can_upgrade_hold = yes NOT = { has_province_flag = hold_@ROOT } }
		set_province_flag = option_hold_15_@ROOT
		set_province_flag = hold_@ROOT
		save_event_target_as = hold15
		setup_digging_deeper_non_capital = yes
	}
	random_owned_province = {
		limit = { can_upgrade_hold = yes NOT = { has_province_flag = hold_@ROOT } }
		set_province_flag = option_hold_16_@ROOT
		set_province_flag = hold_@ROOT
		save_event_target_as = hold16
		setup_digging_deeper_non_capital = yes
	}
	random_owned_province = {
		limit = { can_upgrade_hold = yes NOT = { has_province_flag = hold_@ROOT } }
		set_province_flag = option_hold_17_@ROOT
		set_province_flag = hold_@ROOT
		save_event_target_as = hold17
		setup_digging_deeper_non_capital = yes
	}
	random_owned_province = {
		limit = { can_upgrade_hold = yes NOT = { has_province_flag = hold_@ROOT } }
		set_province_flag = option_hold_18_@ROOT
		set_province_flag = hold_@ROOT
		save_event_target_as = hold18
		setup_digging_deeper_non_capital = yes
	}
	random_owned_province = {
		limit = { can_upgrade_hold = yes NOT = { has_province_flag = hold_@ROOT } }
		set_province_flag = option_hold_19_@ROOT
		set_province_flag = hold_@ROOT
		save_event_target_as = hold19
		setup_digging_deeper_non_capital = yes
	}
	random_owned_province = {
		limit = { can_upgrade_hold = yes NOT = { has_province_flag = hold_@ROOT } }
		set_province_flag = option_hold_20_@ROOT
		set_province_flag = hold_@ROOT
		save_event_target_as = hold20
		setup_digging_deeper_non_capital = yes
	}
	random_owned_province = {
		limit = { can_upgrade_hold = yes NOT = { has_province_flag = hold_@ROOT } }
		set_province_flag = option_hold_21_@ROOT
		set_province_flag = hold_@ROOT
		save_event_target_as = hold21
		setup_digging_deeper_non_capital = yes
	}
	random_owned_province = {
		limit = { can_upgrade_hold = yes NOT = { has_province_flag = hold_@ROOT } }
		set_province_flag = option_hold_22_@ROOT
		set_province_flag = hold_@ROOT
		save_event_target_as = hold22
		setup_digging_deeper_non_capital = yes
	}
	random_owned_province = {
		limit = { can_upgrade_hold = yes NOT = { has_province_flag = hold_@ROOT } }
		set_province_flag = option_hold_23_@ROOT
		set_province_flag = hold_@ROOT
		save_event_target_as = hold23
		setup_digging_deeper_non_capital = yes
	}
	random_owned_province = {
		limit = { can_upgrade_hold = yes NOT = { has_province_flag = hold_@ROOT } }
		set_province_flag = option_hold_24_@ROOT
		set_province_flag = hold_@ROOT
		save_event_target_as = hold24
		setup_digging_deeper_non_capital = yes
	}
	random_owned_province = {
		limit = { can_upgrade_hold = yes NOT = { has_province_flag = hold_@ROOT } }
		set_province_flag = option_hold_25_@ROOT
		set_province_flag = hold_@ROOT
		save_event_target_as = hold25
		setup_digging_deeper_non_capital = yes
	}
	random_owned_province = {
		limit = { can_upgrade_hold = yes NOT = { has_province_flag = hold_@ROOT } }
		set_province_flag = option_hold_26_@ROOT
		set_province_flag = hold_@ROOT
		save_event_target_as = hold26
		setup_digging_deeper_non_capital = yes
	}
	random_owned_province = {
		limit = { can_upgrade_hold = yes NOT = { has_province_flag = hold_@ROOT } }
		set_province_flag = option_hold_27_@ROOT
		set_province_flag = hold_@ROOT
		save_event_target_as = hold27
		setup_digging_deeper_non_capital = yes
	}
	random_owned_province = {
		limit = { can_upgrade_hold = yes NOT = { has_province_flag = hold_@ROOT } }
		set_province_flag = option_hold_28_@ROOT
		set_province_flag = hold_@ROOT
		save_event_target_as = hold28
		setup_digging_deeper_non_capital = yes
	}
	random_owned_province = {
		limit = { can_upgrade_hold = yes NOT = { has_province_flag = hold_@ROOT } }
		set_province_flag = option_hold_29_@ROOT
		set_province_flag = hold_@ROOT
		save_event_target_as = hold29
		setup_digging_deeper_non_capital = yes
	}

}

setup_digging_deeper_non_capital = {
	hidden_effect = {
		trigger_switch = {
			on_trigger = has_province_modifier
			
			dig_1 = { set_variable = { speed = 2 } }
			dig_2 = { set_variable = { speed = 2 } }
			dig_3 = { set_variable = { speed = 3 } }
			dig_4 = { set_variable = { speed = 4 } }
			dig_5 = { set_variable = { speed = 5 } }
			dig_6 = { set_variable = { speed = 6 } }
			dig_7 = { set_variable = { speed = 7 } }
			dig_8 = { set_variable = { speed = 8 } }
			dig_9 = { set_variable = { speed = 8 } }
			dig_10 = { set_variable = { speed = 9 } }
		}
		if = {
			limit = { owner = { has_country_flag = dig_through } }
			multiply_variable = { speed = 0.8 }
		}
		if = {
			limit = { owner = { any_owned_province = { has_province_modifier = the_harpylen_dam } } }
			multiply_variable = { speed = 0.8 }
		}
		if = {
			limit = { owner = { tag = I24 } } #Aul-Dwarov
			multiply_variable = { speed = 0.8 }
		}
	}
}


start_digging_deeper_non_capital = {
	hidden_effect = { cancel_construction = yes }
	if = {
		limit = { check_variable = { speed = 9 } }
		hidden_effect = { add_building_construction = { building = digging_deeper speed = 9 cost = 81 } }
		custom_tooltip = cost_capital_dig_tooltip_9
	}
	else_if = {
		limit = { check_variable = { speed = 8 } }
		hidden_effect = { add_building_construction = { building = digging_deeper speed = 8 cost = 64 } }
		custom_tooltip = cost_capital_dig_tooltip_8
	}
	else_if = {
		limit = { check_variable = { speed = 7.5 } }
		hidden_effect = { add_building_construction = { building = digging_deeper speed = 7.5 cost = 56.25 } }
		custom_tooltip = cost_capital_dig_tooltip_7.5
	}
	else_if = {
		limit = { check_variable = { speed = 7 } }
		hidden_effect = { add_building_construction = { building = digging_deeper speed = 7 cost = 49 } }
		custom_tooltip = cost_capital_dig_tooltip_7
	}
	else_if = {
		limit = { check_variable = { speed = 6.5 } }
		hidden_effect = { add_building_construction = { building = digging_deeper speed = 6.5 cost = 42.25 } }
		custom_tooltip = cost_capital_dig_tooltip_6.5
	}
	else_if = {
		limit = { check_variable = { speed = 6 } }
		hidden_effect = { add_building_construction = { building = digging_deeper speed = 6 cost = 36 } }
		custom_tooltip = cost_capital_dig_tooltip_6
	}
	else_if = {
		limit = { check_variable = { speed = 5.5 } }
		hidden_effect = { add_building_construction = { building = digging_deeper speed = 5.5 cost = 30.25 } }
		custom_tooltip = cost_capital_dig_tooltip_5.5
	}
	else_if = {
		limit = { check_variable = { speed = 5 } }
		hidden_effect = { add_building_construction = { building = digging_deeper speed = 5 cost = 25 } }
		custom_tooltip = cost_capital_dig_tooltip_5
	}
	else_if = {
		limit = { check_variable = { speed = 4.5 } }
		hidden_effect = { add_building_construction = { building = digging_deeper speed = 4.5 cost = 20.25 } }
		custom_tooltip = cost_capital_dig_tooltip_4.5
	}
	else_if = {
		limit = { check_variable = { speed = 4 } }
		hidden_effect = { add_building_construction = { building = digging_deeper speed = 4 cost = 16 } }
		custom_tooltip = cost_capital_dig_tooltip_4
	}
	else_if = {
		limit = { check_variable = { speed = 3.5 } }
		hidden_effect = { add_building_construction = { building = digging_deeper speed = 3.5 cost = 12.25 } }
		custom_tooltip = cost_capital_dig_tooltip_3.5
	}
	else_if = {
		limit = { check_variable = { speed = 3 } }
		hidden_effect = { add_building_construction = { building = digging_deeper speed = 3 cost = 9 } }
		custom_tooltip = cost_capital_dig_tooltip_3
	}
	else_if = {
		limit = { check_variable = { speed = 2.5 } }
		hidden_effect = { add_building_construction = { building = digging_deeper speed = 2.5 cost = 6.25 } }
		custom_tooltip = cost_capital_dig_tooltip_2.5
	}
	else_if = {
		limit = { check_variable = { speed = 2 } }
		hidden_effect = { add_building_construction = { building = digging_deeper speed = 2 cost = 4 } }
		custom_tooltip = cost_capital_dig_tooltip_2
	}
	else_if = {
		limit = { check_variable = { speed = 1.5 } }
		hidden_effect = { add_building_construction = { building = digging_deeper speed = 1.5 cost = 2.25 } }
		custom_tooltip = cost_capital_dig_tooltip_1.5
	}
	else_if = {
		limit = { check_variable = { speed = 1 } }
		hidden_effect = { add_building_construction = { building = digging_deeper speed = 1 cost = 1 } }
		custom_tooltip = cost_capital_dig_tooltip_1
	}
	else_if = {
		limit = { check_variable = { speed = 0 } }
		hidden_effect = { add_building_construction = { building = digging_deeper speed = 1 cost = 1 } }
		custom_tooltip = cost_capital_dig_tooltip_1
	}
}

clr_digging_menu = {
	every_owned_province = {
		limit = { has_province_flag = hold_@ROOT }
		clr_province_flag = option_hold_1_@ROOT
		clr_province_flag = option_hold_2_@ROOT
		clr_province_flag = option_hold_3_@ROOT
		clr_province_flag = option_hold_4_@ROOT
		clr_province_flag = option_hold_5_@ROOT
		clr_province_flag = option_hold_6_@ROOT
		clr_province_flag = option_hold_7_@ROOT
		clr_province_flag = option_hold_8_@ROOT
		clr_province_flag = option_hold_9_@ROOT
		clr_province_flag = option_hold_10_@ROOT
		clr_province_flag = option_hold_11_@ROOT
		clr_province_flag = option_hold_12_@ROOT
		clr_province_flag = option_hold_13_@ROOT
		clr_province_flag = option_hold_14_@ROOT
		clr_province_flag = option_hold_15_@ROOT
		clr_province_flag = option_hold_16_@ROOT
		clr_province_flag = option_hold_17_@ROOT
		clr_province_flag = option_hold_18_@ROOT
		clr_province_flag = option_hold_19_@ROOT
		clr_province_flag = option_hold_20_@ROOT
		clr_province_flag = option_hold_21_@ROOT
		clr_province_flag = option_hold_22_@ROOT
		clr_province_flag = option_hold_23_@ROOT
		clr_province_flag = option_hold_24_@ROOT
		clr_province_flag = option_hold_25_@ROOT
		clr_province_flag = option_hold_26_@ROOT
		clr_province_flag = option_hold_27_@ROOT
		clr_province_flag = option_hold_28_@ROOT
		clr_province_flag = option_hold_29_@ROOT
		clr_province_flag = hold_@ROOT
	}
	clr_country_flag = hold_operation_menu
}

upgrade_hold = {
	trigger_switch = {
		on_trigger = has_province_modifier
		
		dig_1 = { remove_province_modifier = dig_1 add_permanent_province_modifier = { name = dig_2 duration = -1 } }
		dig_2 = { remove_province_modifier = dig_2 add_permanent_province_modifier = { name = dig_3 duration = -1 } }
		dig_3 = { remove_province_modifier = dig_3 add_permanent_province_modifier = { name = dig_4 duration = -1 } }
		dig_4 = { remove_province_modifier = dig_4 add_permanent_province_modifier = { name = dig_5 duration = -1 } }
		dig_5 = { remove_province_modifier = dig_5 add_permanent_province_modifier = { name = dig_6 duration = -1 } }
		dig_6 = { remove_province_modifier = dig_6 add_permanent_province_modifier = { name = dig_7 duration = -1 } }
		dig_7 = { remove_province_modifier = dig_7 add_permanent_province_modifier = { name = dig_8 duration = -1 } }
		dig_8 = { remove_province_modifier = dig_8 add_permanent_province_modifier = { name = dig_9 duration = -1 } }
		dig_9 = { remove_province_modifier = dig_9 add_permanent_province_modifier = { name = dig_10 duration = -1 } }
		dig_10 = { remove_province_modifier = dig_10 add_permanent_province_modifier = { name = dig_11 duration = -1 } }
	}
}


















