government = tribal
add_government_reform = roaming_horde
government_rank = 1
primary_culture = east_sandfang_gnoll
religion = xhazobkult
technology_group = tech_gnollish
national_focus = DIP
capital = 687

1403.2.15 = {
	monarch = {
		name = "Tluukt"
		dynasty = "Cleaver-of-Realms"
		birth_date = 1381.3.15
		adm = 1
		dip = 0
		mil = 4
	}
	add_ruler_personality = mage_personality
}