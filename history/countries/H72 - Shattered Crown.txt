government = tribal
add_government_reform = dwarovar_squatter
government_rank = 1
primary_culture = black_orc
religion = great_dookan
technology_group = tech_orcish
capital = 754


1442.3.17 = {
	monarch = {
		name = "Ghonor"
		dynasty = "Bordu"
		birth_date = 1412.11.4
		adm = 2
		dip = 1
		mil = 6
	}
}