country_decisions = {
	contribute_to_the_dragon_cult_hoard = {
	
		potential = {
			religion = kobold_dragon_cult
			NOT = { has_country_flag = kobold_hoard_menu }
			NOT = { has_global_flag = dragon_roar }
			NOT = { has_country_modifier = kobold_hoard_timer }
		}

		allow = {
			owns = 189
		}
		
		effect = {
			country_event = { id = flavor_kobildzan.3 }
			hidden_effect = { set_country_flag = kobold_hoard_menu }
			if = {
				limit = { ai = yes }
				add_country_modifier = {
					name = kobold_hoard_timer
					duration = 1825
				}
			}
		}
	}

	kobold_nation = {
		major = yes
		potential = {
			culture_group = kobold
			normal_or_historical_nations = yes
			was_never_end_game_tag_trigger = yes
			NOT = { exists = Z38 } 
			capital_scope = { continent = europe }
		}
		allow = {
			
			
			adm_tech = 10
			
			is_free_or_tributary_trigger = yes
			is_nomad = no
			is_at_war = no
			
			OR = {
				owns = 173
				owns = 197
			}
			owns = 189
			
			if = {
				limit = {
					tag = A26
				}
				NOT = { exists = A27 } 
				NOT = { exists = A28 } 
			}
			else_if = {
				limit = {
					tag = A27
				}
				NOT = { exists = A26 } 
				NOT = { exists = A28 } 
			}
			else_if = {
				limit = {
					tag = A28
				}
				NOT = { exists = A26 } 
				NOT = { exists = A27 } 
			}
			else_if = {
				limit = {
					culture_group = kobold
				}
				NOT = { exists = A26 } 
				NOT = { exists = A27 } 
				NOT = { exists = A28 } 
			}
		}
		effect = {
			189 = {
				move_capital_effect = yes
				add_base_tax = 4
				add_base_production = 4
				add_base_manpower = 4
			}
			
			
			change_tag = Z38
			swap_non_generic_missions = yes
			custom_tooltip = tooltip_kobildzani_culture_provinces
			hidden_effect = {
				every_owned_province = {
					limit = {
						culture_group = kobold
					}
					change_culture = kobildzani_kobold
				}
			}
			change_primary_culture = kobildzani_kobold
			
			remove_non_electors_emperors_from_empire_effect = yes
			
			if = {
				limit = {
					NOT = { government_rank = 3 }
				}
				set_government_rank = 3
			}
			
			dragon_coast_region = {
				limit = {
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = Z38
			}
			
			add_prestige = 25
			if = {
				limit = { has_custom_ideas = no }
				country_event = { id = ideagroups.1 } #Swap Ideas
			}
			add_country_modifier = {
				name = "centralization_modifier"
				duration = 7300
			}
			
			set_country_flag = formed_kobildzan_flag
			country_event = { id = anb_miscevents.9 days = 1 } #gov type change event
			
			set_country_flag = locked_racial_administration
			custom_tooltip = locked_racial_administration_tt
		}
		
		ai_will_do = {
			factor = 1
		}
		
		ai_importance = 400
	}
	
	steal_the_kobold_hoard = {
	
		potential = {
			189 = { discovered_by = ROOT }
			NOT = { religion = kobold_dragon_cult }
			REB = { check_variable = { dragon_cult_hoard = 1000 } }
		}

		allow = {
			189 = { controlled_by = ROOT }
		}
		
		effect = {
			if = {
				limit = { REB = { check_variable = { dragon_cult_hoard = 75000 } } }
				add_treasury = 75000
			}
			else_if = {
				limit = { REB = { check_variable = { dragon_cult_hoard = 70000 } } }
				add_treasury = 70000
			}
			else_if = {
				limit = { REB = { check_variable = { dragon_cult_hoard = 65000 } } }
				add_treasury = 65000
			}
			else_if = {
				limit = { REB = { check_variable = { dragon_cult_hoard = 65000 } } }
				add_treasury = 65000
			}
			else_if = {
				limit = { REB = { check_variable = { dragon_cult_hoard = 60000 } } }
				add_treasury = 60000
			}
			else_if = {
				limit = { REB = { check_variable = { dragon_cult_hoard = 55000 } } }
				add_treasury = 55000
			}
			else_if = {
				limit = { REB = { check_variable = { dragon_cult_hoard = 50000 } } }
				add_treasury = 50000
			}
			else_if = {
				limit = { REB = { check_variable = { dragon_cult_hoard = 45000 } } }
				add_treasury = 45000
			}
			else_if = {
				limit = { REB = { check_variable = { dragon_cult_hoard = 40000 } } }
				add_treasury = 40000
			}
			else_if = {
				limit = { REB = { check_variable = { dragon_cult_hoard = 35000 } } }
				add_treasury = 35000
			}
			else_if = {
				limit = { REB = { check_variable = { dragon_cult_hoard = 30000 } } }
				add_treasury = 30000
			}
			else_if = {
				limit = { REB = { check_variable = { dragon_cult_hoard = 25000 } } }
				add_treasury = 25000
			}
			else_if = {
				limit = { REB = { check_variable = { dragon_cult_hoard = 20000 } } }
				add_treasury = 20000
			}
			else_if = {
				limit = { REB = { check_variable = { dragon_cult_hoard = 15000 } } }
				add_treasury = 15000
			}
			else_if = {
				limit = { REB = { check_variable = { dragon_cult_hoard = 10000 } } }
				add_treasury = 10000
			}
			else_if = {
				limit = { REB = { check_variable = { dragon_cult_hoard = 5000 } } }
				add_treasury = 5000
			}
			else_if = {
				limit = { REB = { check_variable = { dragon_cult_hoard = 2500 } } }
				add_treasury = 2500
			}
			else_if = {
				limit = { REB = { check_variable = { dragon_cult_hoard = 1000 } }
				add_treasury = 1000 }
			}
			189 = {
				remove_province_modifier = kobold_hoard_1
				remove_province_modifier = kobold_hoard_2
				remove_province_modifier = kobold_hoard_3
				remove_province_modifier = kobold_hoard_4
				remove_province_modifier = kobold_hoard_5
				remove_province_modifier = kobold_hoard_6
				remove_province_modifier = kobold_hoard_7
				remove_province_modifier = kobold_hoard_8
				remove_province_modifier = kobold_hoard_9
				remove_province_modifier = kobold_hoard_10
				remove_province_modifier = kobold_hoard_11
			}
			hidden_effect = { REB = { set_variable = { dragon_cult_hoard = 0 } } }
		}
	}
}

